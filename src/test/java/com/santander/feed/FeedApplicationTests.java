package com.santander.feed;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.feed.flow.CsvSource;
import com.santander.feed.flow.FlowTransformationService;
import com.santander.feed.model.FeedItem;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class FeedApplicationTests {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private FlowTransformationService flowTransformationService;
  @Test
  void testParseCSV() throws Exception{
    String str = "1, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001";
    FeedItem feedItem = flowTransformationService.transformCSV(str);
    assertEquals(1, feedItem.getId().longValue());
    assertEquals("EUR/USD", feedItem.getInstrument());
    assertNotNull(feedItem.getBid());
    assertNotNull(feedItem.getAsk());
    assertNotNull(feedItem.getDate());
    return;
  }

  @Test
  void testTransformBidAsk() throws Exception{
    String str = "1, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001";
    FeedItem feedItem = flowTransformationService.transformCSV( str);

    double afterBid = feedItem.getBid() + feedItem.getBid() * -0.001;
    double afterAsk = feedItem.getAsk() + feedItem.getAsk() * 0.001;
    feedItem = flowTransformationService.applySpreads(feedItem);
    assertEquals(afterBid, feedItem.getBid().doubleValue(),1);
    assertEquals(afterAsk, feedItem.getAsk().doubleValue(),1);
  }

  @Test
  void testCSVSource(){
    CsvSource csvSource = new CsvSource();
    assertNotNull(csvSource.generate());
  }

  @Test
  void testGetLatest_USDXAU() throws Exception{
    MvcResult result = mvc.perform(MockMvcRequestBuilders
        .get("/instruments/USD/XAU").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andDo(print()).andReturn();

    String content = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    FeedItem feedItem = mapper.readValue(content, FeedItem.class);
    assertNotNull(feedItem.getId());
    assertNotNull(feedItem.getInstrument());
    assertNotNull(feedItem.getBid());
    assertNotNull(feedItem.getAsk());
    assertNotNull(feedItem.getDate());
  }
}
