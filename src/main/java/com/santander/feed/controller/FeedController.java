package com.santander.feed.controller;

import com.santander.feed.model.FeedItem;
import com.santander.feed.repository.FeedItemRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeedController {

  final FeedItemRepository feedItemRepository;

  public FeedController(FeedItemRepository feedItemRepository) {
    this.feedItemRepository = feedItemRepository;
  }

  /**
   *
   * @param c1 First currency
   * @param c2 Second currency
   * @return the latest instrument for the currencies passed on as parameters
   */
  @GetMapping(path = "/instruments/{c1}/{c2}")
  public FeedItem get(@PathVariable String c1, @PathVariable String c2) {
    return feedItemRepository.findTopByInstrumentOrderByDateDesc(c1+"/" + c2).get(0);
  }

}