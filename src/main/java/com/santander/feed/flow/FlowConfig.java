package com.santander.feed.flow;

import com.santander.feed.model.FeedItem;
import com.santander.feed.repository.FeedItemRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.*;

@Configuration
@EnableIntegration
public class FlowConfig {
  private final FeedItemRepository feedItemRepository;

  private final FlowTransformationService flowTransformationService;

  public FlowConfig(FeedItemRepository feedItemRepository, FlowTransformationService flowTransformationService) {
    this.feedItemRepository = feedItemRepository;
    this.flowTransformationService = flowTransformationService;
  }

   public CsvSource csvSource() {
    return new CsvSource();
  }

  @Bean
  public IntegrationFlow instrumentFlow() {
    return IntegrationFlows.from(this.csvSource()::generate,
        c -> c.poller(Pollers.fixedRate(10)))
        .transform(String.class,
            flowTransformationService::transformCSV) // transforms the csv line into a FeedItem object
        .transform(FeedItem.class,
            flowTransformationService::applySpreads) // Applies the spread into the feed item
        .handle((p,h) -> {
          feedItemRepository.save((FeedItem) p); // Persists into DB the transformed feed item
          return p;
        })
        .handle(System.out::println)
        .get();
  }

}
