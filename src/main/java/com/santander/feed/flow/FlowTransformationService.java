package com.santander.feed.flow;

import com.santander.feed.model.FeedItem;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
public class FlowTransformationService {

  /**
   *
   * @params The CSV line
   * @return Returns a FeedItem object based on the CSV line
   */
  @SneakyThrows
  public FeedItem transformCSV(String s){
    return new FeedItem(s);
  }

  /**
   *
   * @param feedItem
   * @return the FeedItem with the spreads applied
   */
  public FeedItem applySpreads(FeedItem feedItem) {
    Double bid = feedItem.getBid();
    bid += bid * -0.001;
    feedItem.setBid(bid);
    Double ask = feedItem.getAsk();
    ask += ask * 0.001;
    feedItem.setAsk(ask);
    return feedItem;
  }
}
