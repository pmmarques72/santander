package com.santander.feed.flow;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class CsvSource {

  AtomicInteger id = new AtomicInteger(0);
  String[] instruments = {"USD/EUR", "USD/XAU", "USD/GBP"};
  SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS");

  public String generate(){
    Random random = new Random();
    int selectedInstrument = random.nextInt(instruments.length);
    double bid = random.nextDouble() * (2 - 1) + 1;
    double ask = random.nextDouble() * (2 - bid) + bid;

    return String.format("%d,%s,%.2f,%.2f,%s", id.incrementAndGet(), instruments[selectedInstrument], bid, ask, formatter.format(new Date()));
  }
}
