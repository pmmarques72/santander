package com.santander.feed.repository;

import java.util.List;

import com.santander.feed.model.FeedItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedItemRepository extends CrudRepository<FeedItem, Long> {

  List<FeedItem> findTopByInstrumentOrderByDateDesc(String instrument);

}