package com.santander.feed.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@Table(name = "FeedItem")
public class FeedItem {

  public FeedItem(String str) throws ParseException {
    String[] elements = str.split(",");
    id = Long.parseLong(elements[0]);
    instrument = elements[1].trim();
    bid = Double.parseDouble(elements[2]);
    ask = Double.parseDouble(elements[3]);
    SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS");
    date = formatter.parse(elements[4]);
    return;
  }

  @Id
  private Long id;

  private String instrument;

  private Double bid;

  private Double ask;

  private Date date;

}
