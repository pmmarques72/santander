# FEED

### Notes

* I opted to get the feed from a generator that I implemented on com.santander.feed.flow.CsvSource. 
  That source is feeding a spring integration flow and could be changed to anything else by changing one line
  this.csvSource()::generate,c -> c.poller(Pollers.fixedRate(10))
  It could be changed to consume a queue like Amazon SQS, MQ, etc
* Tests are implemented for all steps of the spring integration flow (parsing, applying spreads)
* At the end of the flow the current item is persisted in a in-memory DB
* Endpoint created to get the latest value for a given instrument. E.g localhost:/8080/instruments/USD/XAU
* As soon as the application starts random data starts being generated

